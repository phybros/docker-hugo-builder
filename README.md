# docker-hugo-builder

A Docker image for building/minifying hugo websites.

# Usage

In Bitbucket Pipelines, the image can be used like this:

```yaml
image: phybros/docker-hugo-builder:0.83.1
pipelines:
  branches:
    main:
      - step:
          name: Build and minify files
          script:
            - hugo --config my-config.toml
            - minify -r -o public/ public/
          artifacts:
            - public/**
```

# Development

To create new docker tags, make your changes tag the repo and push up,
and Dockerhub will do the rest (including making a new image tag).
